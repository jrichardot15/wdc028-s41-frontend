import { useEffect, useState } from 'react'
import moment from 'moment'
import { Tabs, Tab } from 'react-bootstrap'
import MonthlyChart from '../components/MonthlyChart'

export default function insights(){
	const [distances, setDistances] = useState([]),
	const [durations, setDurations] = useState([]),
	const [charges, setCharges] = useState([])

	useEffect(() =>{	
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.travels.length > 0){
				let monthlyDistance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyDuration = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyCharge = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

				data.travels.forEach(travel => {
					const index = moment(travel.date).month()

					monthlyDistance[index] += (travel.distance/1000),
					monthlyDuration[index] += (travel.duration/3600),
					monthlyCharge[index] += (travel.charge.amount)
				})

				setDistances(monthlyDistance),
				setDurations(monthlyDuration),
				setCharges(monthlyCharge)
			}
		})
	}, [])

	return(
		<Tabs defaultActiveKey="distances" id="monthlyFigures">
			<Tab eventKey="distances" title="Monthly Distance Travelled">
			<MonthlyChart figuresArray={distances} label={"Monthly total in kilometes"}/>
			</Tab>
			<Tab eventKey="durations" title="Monthly Time Travelled">
			<MonthlyChart figuresArray={durations} label={"Monthly total in hours"}/>
			</Tab>
			<Tab eventKey="charges" title="Monthly Travelled Charges">
			<MonthlyChart figuresArray={charges} label={"Monthly total in Php"}/>
			</Tab>			
		</Tabs>
	)
}