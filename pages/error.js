import Head from 'next/head'
import Banner from '../components/Banner'

export default function error() {
  return (
    <>
      <Head>
        <title>Oops...</title>
      </Head>
      <Banner title="Something went wrong" content="Please try again later." />
    </>
  )
}